const express = require('express');
const router = express.Router();
const verifyJWT = require('../middlewares/verifyToken');

const {getUser, deleteUser, updateUser} = require('../controllers/manageUser')

router.get('/me', verifyJWT, getUser);
router.delete('/me', verifyJWT, deleteUser)
router.patch('/me', verifyJWT, updateUser)

module.exports = router;
