require('dotenv').config();
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const morgan = require('morgan');
const PORT = process.env.PORT || 8080;

const errorHandler = require('./middlewares/errorHandler');
const authRoute = require('./routes/authRoute');
const usersRoute = require('./routes/usersRoute');
const notesRoute = require('./routes/notesRoute');

app.use(express.json());
app.use(morgan('tiny'));

const connectDB = async () => {
    try {
        await mongoose.connect(process.env.DATABASE_URI, {
            useUnifiedTopology: true,
            useNewUrlParser: true
        });
    } catch (err) {
        console.log(`Failed to connect: ${err.message}`);
    }
}

connectDB();

mongoose.connection.once('open', () => {
    console.log('Connected to db');
    app.listen(PORT, () => {
        console.log(`Server started at port ${PORT}`);
    })
});

// routes
app.use('/api/auth', authRoute);
app.use('/api/users', usersRoute);
app.use('/api/notes', notesRoute);

//ERROR HANDLER
app.use(errorHandler)
