const User = require('../models/User');
const bcrypt = require('bcryptjs');

async function getUser(req, res) {
    try {
        const user = await User.findById({ _id: req.user._id });
        if (!user) {
            return res.status(400).json({"message": "not found"});
        }
        const {_id, username, createdDate} = user;
        res.status(200).json({"user": {_id, username, createdDate}});
    } catch (error) {
        res.status(500).json({"message": error.message});
    }
}

async function deleteUser(req, res) {
    try {
        const deletedUser = await User.findByIdAndDelete({ _id: req.user._id });
        if (!deletedUser) {
            return res.status(400).json({"message": "not found"});
        }
        res.status(200).json({"message": "Success"});
    } catch (error) {
        res.status(500).json({"message": error.message});
    }
}

async function updateUser(req, res) {
    try {
        const {oldPassword, newPassword} = req.body;
        const user = await User.findById({ _id: req.user._id });
        const match = await bcrypt.compare(oldPassword, user.password);
        console.log('match', match);
        const hashedPass = await bcrypt.hash(newPassword, 10);

        const updatedUser = await User.findByIdAndUpdate(req.user._id, {password: hashedPass});
        if (!updatedUser) {
            return res.status(400).json({"message": "not found"});
        }
        res.status(200).json({"message": "Success"});
    } catch (error) {
        res.status(500).json({"message": error.message});
    }
}

module.exports = {
    getUser,
    deleteUser,
    updateUser
}
